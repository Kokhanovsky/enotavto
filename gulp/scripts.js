'use strict';

var gulp = require('gulp');

var $ = require('gulp-load-plugins')();

// compile typescript
gulp.task('scripts', function () {
    var tsResult = gulp.src('ts/app.ts')
        .pipe($.sourcemaps.init())
        .pipe($.typescript({
            noImplicitAny: true,
            out: 'app.js'
        }));
    return tsResult.js
        .pipe($.uglify())
        .pipe($.rename({
            suffix: ".min"
        }))
        .pipe($.sourcemaps.write('.', {includeContent: false, sourceRoot: '/ts'}))
        .pipe(gulp.dest('js'));
});