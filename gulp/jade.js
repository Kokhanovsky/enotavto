'use strict';

var gulp = require('gulp');
var j = require('gulp-jade');

gulp.task('jade', function() {
    gulp.src(['./jade/**/*.jade'])
        .pipe(j({
            pretty: true
        }))
        .pipe(gulp.dest('./'))
    ;
});