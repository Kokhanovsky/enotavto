'use strict';

var gulp = require('gulp');

var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'main-bower-files']
});

var jsFilter = $.filter(['**/*.js']);
//var jsFilter = $.filter(['angular.js', 'jquery.js', 'angular-resource.js', 'ui-bootstrap-tpls.js']);

gulp.task('bower-scripts', function () {
    return gulp.src($.mainBowerFiles())
        .pipe(jsFilter)
        .pipe($.sourcemaps.init())
        .pipe(gulp.dest('./.tmp/bower_js'))
        .pipe($.clean())
        .pipe($.concat('vendor.min.js'))
        .pipe($.uglify())
        .pipe($.sourcemaps.write('.', {sourceRoot: '/vendor'}))
        .pipe(gulp.dest('./js'));
});