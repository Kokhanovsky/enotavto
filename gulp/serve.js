'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync').create();


gulp.task('serve', function() {
    browserSync.init(["css/*.css", "**/*.html"], {
        server: {
            baseDir: "./"
        }
    });
    gulp.watch("jade/**/*.jade", ['jade']);
    gulp.watch("sass/**/*.sass", ['sass']);
    gulp.watch("sass/**/*.scss", ['sass']);
    gulp.watch('ts/**/*.ts', ['scripts']).on('change', browserSync.reload);
});