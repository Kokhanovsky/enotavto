'use strict';

var path = require('path');
var gulp = require('gulp');

var $ = require('gulp-load-plugins')();


// compile sass
gulp.task('sass', function () {
    gulp.src(['./sass/styles.sass'])
        .pipe($.sourcemaps.init())
        .pipe($.sass({outputStyle: 'compressed'}).on('error', errorHandler))
        .pipe($.sourcemaps.write('./'))
        .pipe(gulp.dest('./css'));
});

// Handle the error
function errorHandler (error) {
    console.log(error.toString());
    this.emit('end');
}