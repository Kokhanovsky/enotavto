<?php
include("constants.php");

header('Content-type: application/json');
$request_body = file_get_contents('php://input');
$data = json_decode($request_body);

$sign = Product_id."-".$data->amount."-".Shared_sec;

if ($data->amount) {
    print json_encode([
        "product_id" => Product_id,
        "sign" => md5(Product_id."-".$data->amount."-".Shared_sec)    
    ]);
}

/*
<form action="https://payments.chronopay.com/" method="POST">
<input type="hidden" name="product_id" value="001234-0001-0001" />
<input type="hidden" name="product_price" value="12.34" />
<input type="hidden" name="cs1" value="client value 1" />
<input type="hidden" name="cs2" value="client value 2" />
<input type="hidden" name="cs2" value="client value 3" />
<input type="hidden" name="cb_url" value="https://example.com/callback/" />
<input type="hidden" name="success_url" value="https://example.com/payment_success/" />
<input type="hidden" name="decline_url" value="https://example.com/payment_failed/" />
<input type="hidden" name="sign" value="d41d8cd98f00b204e9800998ecf8427e" />
<input type="submit" value="Оплатить через ChronoPay" />
</form>*/ 