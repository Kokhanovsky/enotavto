<?php
if(!defined('allow_access')) {
    die('Direct access not permitted');
}

function get_order_number() {
    $counter_file_name = "order_number/order_number.txt";

    if (!file_exists($counter_file_name)) {
        // Reopen the file and erase the contents
        fopen($counter_file_name, "w+");
    }

    $fp = fopen($counter_file_name, "r");

    // Get the existing count
    $count = (int)(fread($fp, 1024));

    // Close the file
    fclose($fp);

    // Add 1 to the existing count
    $count = $count + 1;

    // Reopen the file and erase the contents
    $fp = fopen($counter_file_name, "w");

    // Write the new count to the file
    fwrite($fp, $count);

    // Close the file
    fclose($fp);

    return sprintf('%05d', $count);
}