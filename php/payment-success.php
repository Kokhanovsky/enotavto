<?php
include ("constants.php");
require '../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

// check remote IP, must be valid IP from Chronopay
if (Chronopay_ip != $_SERVER['REMOTE_ADDR'])
    die;

$sign = md5(Shared_sec.$_GET['customer_id'].$_GET['transaction_id'].$_GET['transaction_type'].$_GET['total']);

// check signature
if ($sign != $_GET['sign'])
    die;

function create_mailer() {
    $mail = new PHPMailer;
    $mail->CharSet = "UTF-8";
    $mail->setFrom(Noreply_email, 'ЕнотАвто (отчет об оплате)');
    return $mail;
}

function sendMessage($data) {
    if (empty($data)) {
        die("Empty data");
    }
    $mail = create_mailer();
    $mail->IsHTML(true);
    
    foreach(unserialize(Send_to) as $st) {
        $mail->addAddress($st);
    }
    $mail->Subject = 'Успешная оплата заказа '.$data['cs2'];
    $body = "";
    $body .= "Номер заказа: <b>".$data['cs2']."</b><br>";
    $body .= "Сумма платежа: <b>".$data['total']."</b><br>";
    if (!empty($data['cs1']))
        $body .= "Примечание к платежу: ".$data['cs1']."<br>";
    $body .= "<br>";
    foreach ($data as $key => $value) {
        switch($key) {
            case "transaction_id":
                $body .= "Идентификатор транзакции: ".$value."<br>";
                break;
            case "transaction_type":
                $body .= "Тип транзакции: ".$value."<br>";
                break;
            case "date":
                $body .= "Дата и время: ".$value."<br>";
                break;
            case "currency":
                $body .= "Валюта платежа: ".$value."<br>";
                break;
            case "customer_id":
                $body .= "Идентификатор покупателя: ".$value."<br>";
                break;
            case "ip":
                $body .= "ip-адрес плательщика: ".$value."<br>";
                break;
            case "payment_type":
                $body .= "Тип платежного средства: ".$value."<br>";
                break;
            case "email":
                $body .= "Адрес электронной почты Покупателя: ".$value."<br>";
                break;
            case "country":
                $body .= "Страна Покупателя: ".$value."<br>";
                break;
            case "name":
                $body .= "Имя и фамилия держателя Платежного инструмента (Банковской карты): ".$value."<br>";
                break;
            case "creditcardnumber":
                $body .= "Номер Карты: ".$value."<br>";
                break;
            case "cardholder":
                $body .= "Имя и Фамилия держателя, указанная на Карте: ".$value."<br>";
                break;
            case "expire_date":
                $body .= "Срок действия Карты (год/мес): ".$value."<br>";
                break;
            case "eci":
                $body .= "Electronic Commerce Indicator (eci): ".$value."<br>";
                break;
            case "card_bank_name":
                if (empty($value))
                    break;
                $body .= "Название банка эмитента карты: ".$value."<br>";
                break;
            case "auth_code":
                $body .= "Код авторизации: ".$value."<br>";
                break;
        }
    }
    $body .= "<br>Отчет сформирован на Enotavto.ru. Удачи!";
    
    $mail->Body = $body;
    
    $mail->send();
}

sendMessage($_GET);