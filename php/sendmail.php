<?php
define("allow_access", "true");
include("constants.php");

require '../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
require 'order_counter.php';

function create_mailer() {
    $mail = new PHPMailer;
    $mail->CharSet = "UTF-8";
    $mail->setFrom(Noreply_email, 'ЕнотАвто (вебсайт)');
    return $mail;
}

function sendMessage($fio, $email, $content) {
    if (empty($fio) || empty($email) || empty($content)) {
        die("Empty message fields");
    }
    $mail = create_mailer();
    foreach(unserialize(Send_to) as $st) {
        $mail->addAddress($st);
    }
    $mail->Subject = 'Сообщение или вопрос с сайта ЕнотАвто';
    $body = "Имя отправителя: " . $fio . "\n\n";
    $body .= "E-Mail: " . $email . "\n\n";
    $body .= "Текст сообщения: " . $content . "\n\n";
    $mail->Body    = $body;
    $mail->send();
}

function sendOrder($fio, $email, $phoneNumber, $partsName, $partsNumber, $carMark, $carModel, $vin, $volume, $year, $install) {
    $mail = create_mailer();
    if (empty($fio) || empty($email) || 
    
        (!(!empty($partsName) && !empty($carMark) && !empty($carModel) && !empty($vin) && !empty($volume) && !empty($year))
        
            &&
        
            (empty($partsNumber)))
    ) {
        die("Empty order fields");
    }
    foreach(unserialize(Send_to) as $st) {
        $mail->addAddress($st);
    }
    $order_number = get_order_number();
    $mail->Subject = 'Заказ с сайта ЕнотАвто '.$order_number;
    $mail->Body = "Номер заказа: " . $order_number . "\n\n";
    $mail->Body .= "Имя отправителя: " . $fio . "\n\n";
    $mail->Body .= "E-Mail: " . $email . "\n\n";

    if (!empty($phoneNumber))
        $mail->Body .= "Номер телефона: " . $phoneNumber . "\n\n";
    
    if ($partsName) {
        $mail->Body .= "Наименование запчастей: " . $partsName . "\n\n";
        $mail->Body .= "Марка автомобиля: " . $carMark . "\n\n";
        $mail->Body .= "Модель автомобиля: " . $carModel . "\n\n";
        $mail->Body .= "VIN: " . $vin . "\n\n";
        $mail->Body .= "Объем двигателя: " . $volume . "\n\n";
        $mail->Body .= "Год выпуска: " . $year . "\n\n";
    }

    if ($partsNumber) {
        $mail->Body .= "Номер(-а) запчастей:\n" . $partsNumber . "\n\n";
    }

    if (!empty($install)) {
        $mail->Body .= "ТРЕБУЕТСЯ УСТАНОВКА ЗАКАЗАННОЙ ЗАПЧАСТИ";
    }
    if ($mail->send() && $email) {
        sendOrderNotice($order_number, $email);
    }
}

function sendOrderNotice($order_number, $email) {
    $mail = create_mailer();
    $mail->addAddress($email);
    $mail->setFrom('info@enotavto.ru', 'ЕнотАвто');
    $mail->Subject = 'Номер вашего заказа '.$order_number;
    $mail->Body = "Вашему заказу присвоен номер $order_number.\n";
    $mail->Body .= "Заявка находится в обработке. Наш менеджер свяжется с Вами в ближайшее время.\n\n";
    $mail->Body .= "--------------------------------------\nЕНОТ - автозапчасти";
    $mail->send();
}

$request_body = file_get_contents('php://input');
$data = json_decode($request_body);

if (empty($data->messageType)) {
    die('Empty message');
}

switch ($data->messageType) {
    case "order":
        sendOrder($data->fio,
                  $data->email,
                  @$data->phoneNumber,
                  @$data->partsName,
                  @$data->partsNumber,
                  @$data->carMark,
                  @$data->carModel,
                  @$data->vin,
                  @$data->volume,
                  @$data->year,
                  @$data->install);
        break;
    case "message":
        sendMessage($data->fio, $data->email, $data->content);
        break;
}


print true;