// TYPINGS

/// <reference path="../typings/angularjs/angular.d.ts" />
/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/angular-ui-bootstrap/angular-ui-bootstrap.d.ts" />
/// <reference path="../typings/angular-scroll/angular-scroll.d.ts" />

// APP

/// <reference path="app.module.ts" />


// COMMON

/// <reference path="common/common.module.ts" />
/// <reference path="common/common.service.ts" />


// LANDING

/// <reference path="landing/landing.module.ts" />
/// <reference path="landing/landing.controller.ts" />
/// <reference path="landing/landing.service.ts" />


// DIRECTIVES

/// <reference path="directives/directives.module.ts" />
/// <reference path="directives/directives.factory.ts" />
/// <reference path="directives/bg-slides.directives.ts" />
/// <reference path="directives/masked-input.ts" />

/// <reference path="scripts.ts" />
