module app.common {
    'use strict';

    export interface ICommonService {
        $modal: angular.ui.bootstrap.IModalService;
        $http: ng.IHttpService;
        $log: ng.ILogService;
        $timeout: ng.ITimeoutService;
        $window: ng.IWindowService;
    }

    export class commonService implements ICommonService {
        static $inject = ['$uibModal', '$http', '$log', '$timeout', '$window'];
        constructor(
            public $modal: angular.ui.bootstrap.IModalService,
            public $http: ng.IHttpService,
            public $log: ng.ILogService,
            public $timeout: ng.ITimeoutService,
            public $window: ng.IWindowService
        ) {}

    }

    angular
        .module('app.common')
        .service('app.common.commonService', commonService);
}