((): void => {
    "use strict";

    angular.module("app", [
        "app.common",
        "app.directives",
        "app.landing",
        //---------------
        "ui.bootstrap",
        "duScroll",
        "ngAnimate",
    ]);

})();