module app.landing {
    "use strict";

    export interface IOrder {
        messageType: string;
        fio: string;
        email: string;
        phoneNumber: string;
        partsName: string;
        partsNumber: string;
        carMark: string;
        carModel: string;
        vin: string;
        volume: number;
        year: number;
        install: boolean;
        loading: boolean;
    }

    export interface IMessage {
        messageType: string;
        fio: string;
        email: string;
        content: string;
        loading: boolean;
    }

    export interface ILandingService {
        sendOrder(scope: ILandingScope):  void;
        sendPayment(scope: any):  void;
        sendMessage(scope: ILandingScope):  void;
        
        // open modals
        openModal(modalType: string, modalSize?: string, data?: any): angular.ui.bootstrap.IModalServiceInstance;
        openPaymentModal(): angular.ui.bootstrap.IModalServiceInstance;
    }

    class LandingService implements ILandingService {
 
        static $inject = ["app.common.commonService", "$http", "$location"];

        constructor(private commonService:app.common.ICommonService, private http: ng.IHttpService, private $location: ng.ILocationService ) {
        }

        // send order
        sendOrder(scope: ILandingScope) {
            scope.order.loading = true;
            scope.order.messageType = "order";
            this.http.post('/php/sendmail.php', scope.order).then(() => {
                this.openModal("orderSuccess", "md", { name: scope.order.fio });
                scope.order = null;
                scope.orderForm.$setPristine();
            })
            .catch(() => {
                this.openModal("error", "md");
            })
            .finally(() => {
                scope.order.loading = false;
            });
        }

        sendPayment(scope: any):void {
            scope.payment.loading = true;
            scope.payment.messageType = "payment";
            this.http.post('/php/payment.php', scope.payment).then((response: any) => {
                var data = response.data;
                data.product_price = scope.payment.amount;
                data.product_name = "Enotavto.ru: оплата заказа номер " + scope.payment.number;
                data.email = scope.payment.email;
                data.country = "RUS";
                data.cs1 = scope.payment.notice;
                data.cs2 = scope.payment.number;
                data.success_url = "http://enotavto.ru/#/payment-success";
                data.decline_url = "http://enotavto.ru/#/payment-error";
                data.cb_url = "http://enotavto.ru/php/payment-success.php";
                data.cb_type = "G";
                this.openModal("paymentRedirect", "md");
                this.redirectPayment("https://payments.chronopay.com/", data);
                scope.modal.close();
            })
            .catch(() => {
                this.openModal("error", "md");
            })
            .finally(() => {
                scope.payment.loading = false;
                scope.modal.close();
                console.log(scope.modal);
            });
        }
        
        redirectPayment(location: string, args: any):void {
            var form = '';
            $.each( args, function( key, value ) {
                value = !value ? value : value.split('"').join('\"')
                form += '<input type="hidden" name="'+key+'" value="'+value+'">';
            });
            var formElement = $('<form action="' + location + '" method="POST" style="display: none">' + form + '</form>');
            formElement.appendTo($(document.body)).submit();
            formElement.remove();
        }

        sendMessage(scope: ILandingScope) {
            scope.message.loading = true;
            scope.message.messageType = "message";
            this.http.post('/php/sendmail.php', scope.message).then(() => {
                this.openModal("messageSuccess", "md", { name: scope.message.fio });
                scope.message = null;
                scope.messageForm.$setPristine();
            })
            .catch(() => {
                this.openModal("error", "md");
            })
            .finally(() => {
                scope.message.loading = false;
            });
        }
    
        openModal(modalType: string, modalSize = "md", data?: any) {
            var that = this;
            var modal: angular.ui.bootstrap.IModalServiceInstance;
            var controller = function($scope: any) {
                $scope.data = data;
                $scope.closeModal = () => {
                    modal.close();
                };
            };
            controller.$inject = ["$scope"];
            var templateUrl: string;
            switch(modalType)
            {
                case "agreement":
                    templateUrl = "templates/agreement.tpl.html";
                    break;
                case "paymentSuccess":
                    templateUrl = "templates/payment-success.tpl.html";
                    break;
                case "paymentError":
                    templateUrl = "templates/payment-error.tpl.html";
                    break;
                case "orderSuccess":
                    templateUrl = "templates/order-success.tpl.html";
                    break;
                case "messageSuccess":
                    templateUrl = "templates/message-success.tpl.html";
                    break;
                case "instruction":
                    templateUrl = "templates/instruction.tpl.html";
                    break;
                case "error":
                    templateUrl = "templates/error.tpl.html";
                    break;
                case "paymentRedirect":
                    templateUrl = "templates/payment-redirect.tpl.html";
                    break;
            }
            var modal = this.commonService.$modal.open({
                animation: true,
                size: modalSize,
                templateUrl: templateUrl,
                controller: controller
            });
            modal.result.finally(() => {
                that.$location.path("");
            });
            return modal;
        }
        
        openPaymentModal() {
            var that = this;
            var modal: angular.ui.bootstrap.IModalServiceInstance;
            var controller = function($scope: any) {
                $scope.modal = modal;
                $scope.payment = {};
                $scope.openAgreement = () => {
                    that.openModal("agreement", "large")
                    that.$location.path("agreement");
                }
                $scope.closeModal = () => {
                    modal.close();
                };
                $scope.pay = function() {
                    that.sendPayment(this);
                }
            };
            controller.$inject = ["$scope"];
            var modal = this.commonService.$modal.open({
                animation: true,
                size: "md",
                templateUrl: "templates/payment-form.tpl.html",
                controller: controller
            });
            modal.result.finally(() => {
                that.$location.path("");
            });
            return modal;
        }

    }

    angular
        .module("app.landing")
        .service("app.landing.landingService", LandingService);
}
