module app.landing {
    import ILocationService = angular.ILocationService;
    import IDocumentService = duScroll.IDocumentService;
    import IAnchorScrollService = angular.IAnchorScrollService;
    import IAnchorScrollProvider = angular.IAnchorScrollProvider;
    import IWindowService = angular.IWindowService;
    "use strict";

    export interface ILandingController {
        sendOrder(): void;
        sendMessage(): void;
        openInstruction(): void;
        openAgreement(): void;
        openPayment(): void;
    }

    export interface ILandingScope extends ng.IScope {
        order: IOrder;
        orderForm: ng.IFormController;
        message: IMessage;
        messageForm: ng.IFormController;
    }

    class LandingController implements ILandingController {

        static $inject = ["$scope", "app.landing.landingService", "$location", "$document"];
        
        constructor(private landingScope: ILandingScope, private landingService: ILandingService, private $location: ILocationService, private $document: IDocumentService) {
            this._activate();
        }

        sendOrder() {
            this.landingService.sendOrder(this.landingScope);
        }

        sendMessage() {
            this.landingService.sendMessage(this.landingScope);
        }

        openInstruction() {
            this.landingService.openModal("instruction", "large");
            this.$location.path("instruction");
        }
        
        openAgreement() {
            this.landingService.openModal("agreement", "large");
            this.$location.path("agreement");
        }

        openPayment() {
            this.landingService.openPaymentModal();
            this.$location.path("payment");
        }

        private _activate() {
    
            switch(this.$location.path()) {
                case "/agreement":
                    this.landingService.openModal("agreement", "large");
                    break;
                //case "/instruction":
                //    this.landingService.openModal("instruction", "large");
                //    break;
                case "/payment":
                    this.landingService.openPaymentModal();
                    break;
                case "/payment-success":
                    this.landingService.openModal("paymentSuccess");
                    break;
                case "/payment-error":
                    this.landingService.openModal("paymentError");
                    break;
            }

            this.landingScope.order = <IOrder> {
                loading: false
            };
            this.landingScope.message = <IMessage> {
                loading: false
            };
        }
        

    }

    angular.module("app.landing")
        .controller("app.landing.LandingController", LandingController);

}