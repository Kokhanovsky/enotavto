module app.directives {
    'use strict';

    interface IBgSlidesAttrs extends ng.IAttributes {
        bgSlides: string;
        imagesFolder: string;
        transition: number;
        time: number;
    }

    interface IBgSlidesScope extends ng.IAttributes {
        id: number;
        name: string;
    }

    interface IVelocity extends JQuery {
        velocity: any;
    }

    export class bgSlides implements ng.IDirective {
        replace = true;
        link(scope: IBgSlidesScope, element: ng.IAugmentedJQuery, attr: IBgSlidesAttrs): void {
            var images = attr.bgSlides.split(";");
            var transition = attr.transition ? attr.transition : 300;
            var time = attr.time ? attr.time : 5000;
            var el = $(element);
            el.css("position", "relative");
            function animate(div: IVelocity, i: number) {
                setTimeout(() => {
                    div.velocity({
                        "opacity": "1"
                    }, {
                        "duration": transition
                    })
                    .velocity({
                        "opacity": "0"
                    }, {
                        "duration": transition,
                        "delay": time
                    });
                }, (time) * i + (transition ) * i );
            }
            for (var i = 0; i < images.length ; i++) {
                if (attr.imagesFolder) {
                    images[i] = attr.imagesFolder + images[i];
                }
                // preload images
                $("<img />").attr("src", images[i]);
                el.prepend("<div class=\"bg\">");
                var div = <IVelocity> $("div.bg:first-child", el);
                div.css({
                    "position": "absolute",
                    "width": "100%",
                    "height": "100%",
                    "backgroundImage": "url(" + images[i] + ")",
                    "opacity": 0
                });
                animate(div, i);
                setInterval(animate.bind(null, div, i), (time) * images.length + (transition) * images.length);
            }
        }
    }

    angular.module('app.directives')
        .directive("bgSlides", app.directives.getFactoryFor(bgSlides));

}

