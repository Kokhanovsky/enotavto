module app.directives {
    'use strict';

    interface IMaskAttrs extends ng.IAttributes {
        maskedInput: string;
        maskPlaceholder: string;
    }

    export class maskedInput implements ng.IDirective {
        replace = true;
        require = '?ngModel';
        link(scope: ng.IScope, element: any, attr: IMaskAttrs, controller: ng.INgModelController): void {
            var el: any;
                el = $(element);
            //noinspection TypeScriptValidateJSTypes
            el.mask(attr.maskedInput, {
                placeholder: attr.maskPlaceholder,
                completed: () => {
                    controller.$setViewValue(element.val());
                    scope.$apply();
                }
            });
            el.on("change", () => {
                if (!el.val()) {
                    controller.$setViewValue("");
                    scope.$apply();
                }
            });
        }
    }

    angular.module('app.directives')
        .directive("maskedInput", app.directives.getFactoryFor(maskedInput));

}

