module app.directives {
    export function getFactoryFor(classType:Function):ng.IDirectiveFactory {
        var factory = (...args:any[]):ng.IDirective => {
            var newInstance = Object.create(classType.prototype);
            newInstance.constructor.apply(newInstance, args);
            return newInstance;
        }
        factory.$inject = classType.$inject;
        return factory;
    }
}